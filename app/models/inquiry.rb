class Inquiry < ActiveRecord::Base

  validates :name, :presence => true
  validates :message, :presence => true
  validates :company, :presence => true
  validates :email, :presence => true
  
  acts_as_indexed :fields => [:name, :email, :message, :phone, :company, :address, :city, :state, :zip, :country]

  default_scope :order => 'created_at DESC' # previously scope :newest

  def self.latest(number = 7, include_spam = true)
    include_spam ? limit(number) : ham.limit(number)
  end

end
